﻿using ConsoleApp1.Delegate;
using ConsoleApp1.Socket;
using System;
using System.IO;
using System.Text;

namespace ConsoleApp1
{
    public class Server : ServerDelegate
    {

        private ServerSocket serverSocket1;
        private ServerSocket serverSocket2;

        public Server(ClientSocket c1, ClientSocket c2)
        {
            serverSocket1 = new ServerSocket(this, c1);
            serverSocket2 = new ServerSocket(this, c2);
        }

        public void Receive(Message message)
        {
            if (message.type == ClientTypeEnum.CLIENT_1)
            {
                // send message
                serverSocket2.GotMessage(message);
            } else if (message.type == ClientTypeEnum.CLIENT_2)
            {
                // send message
                serverSocket1.GotMessage(message);
            }
        }

        public ServerSocket GetServerSocket1()
        {
            return serverSocket1;
        }

        public ServerSocket GetServerSocket2()
        {
            return serverSocket2;
        } 
    }
}
