﻿using ConsoleApp1.Delegate;
using ConsoleApp1.Socket;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.Browser
{
    class Browser: BrowserDelegate
    {
        private ClientSocket clientSocket;

        public Browser()
        {
            clientSocket = new ClientSocket(this);
        }

        public void SendMessage(string message)
        {
            Message m1 = new Message(message, ClientTypeEnum.CLIENT_1);
            clientSocket.Receive(m1);
        }
        public void GotMessage(Message message)
        {
            if(message.type == ClientTypeEnum.CLIENT_1)
            {
                Console.WriteLine(message.message);
            }
            else if(message.type == ClientTypeEnum.CLIENT_2)
            {
                Console.WriteLine(message.message);
            }
        }

        public ClientSocket GetClientSocket()
        {
            return clientSocket;
        }
    }
}
