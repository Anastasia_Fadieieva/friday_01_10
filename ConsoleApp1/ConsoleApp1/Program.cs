﻿using ConsoleApp1;
using ConsoleApp1.Browser;
using ConsoleApp1.Socket;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

class Program
{
    static void Main(string[] args)
    {
        //ArrayList user1 = new ArrayList();
        Queue<string> user1 = new Queue<string>();
        user1.Enqueue("Hi");
        user1.Enqueue("How are you?");
        user1.Enqueue("I`m fine, thanks");
        user1.Enqueue("Bye");
        
        //ArrayList user2 = new ArrayList();
        Queue<string> user2 = new Queue<string>();
        user2.Enqueue("Hello");
        user2.Enqueue("I am okey) What about you?");
        user2.Enqueue("Good");
        user2.Enqueue("Bye)");

        Browser browser1 = new Browser();
        Browser browser2 = new Browser();

        Server server = new Server(browser1.GetClientSocket(), browser2.GetClientSocket());

        browser1.GetClientSocket().SetServerSocket(server.GetServerSocket1());
        browser2.GetClientSocket().SetServerSocket(server.GetServerSocket2());

       
        int i = user1.Count + user2.Count;

        while (i > 0)
        {
            if (user1.Count > 0)
            {
                Console.ForegroundColor = ConsoleColor.DarkCyan;
                string message1 = user1.Dequeue();
                browser1.SendMessage(message1/*, ClientTypeEnum*/ );
                Thread.Sleep(2500);
                Console.ResetColor();
                i--;
            }

            if (user2.Count > 0)
            {
                Console.ForegroundColor = ConsoleColor.Magenta;
                string message2 = user2.Dequeue();
                browser2.SendMessage(message2);
                Thread.Sleep(2000);
                Console.ResetColor();
                i--;
            }
        }
      
    }
}