﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.Socket
{
    interface IWebSocketServer 
    {
        public void SendMessage(Message message);
    }
}
