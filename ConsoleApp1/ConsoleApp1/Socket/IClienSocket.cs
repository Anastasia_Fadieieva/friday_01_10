﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.Socket
{
    public interface IClientSocket
    {
        public void SendMessage(Message message);
        public void GotMessage(Message message);
    }
}
