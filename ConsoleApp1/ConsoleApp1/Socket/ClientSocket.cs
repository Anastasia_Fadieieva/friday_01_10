﻿using ConsoleApp1.Browser;
using ConsoleApp1.Delegate;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.Socket
{
    public class ClientSocket : IClientSocket, SocketClientDelegate
    {
        private SocketServerDelegate socketServerDelegate;
        private BrowserDelegate browserDelegate;

        public ClientSocket(BrowserDelegate b1)
        {
            browserDelegate = b1;
        }

        public void SendMessage(Message message)
        {
            socketServerDelegate.Receive(message);
        }

        public void Receive(Message message)
        {
            SendMessage(message);
        }

        public void GotMessage(Message message)
        {
            browserDelegate.GotMessage(message);
        }

        public void SetServerSocket(SocketServerDelegate s1)
        {
            socketServerDelegate = s1;
        }
    }
}

