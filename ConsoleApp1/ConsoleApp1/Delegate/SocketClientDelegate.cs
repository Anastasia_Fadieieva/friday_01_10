﻿using ConsoleApp1.Socket;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.Browser
{
    public interface SocketClientDelegate
    {
        public void Receive(Message message);

        public void GotMessage(Message message);
    }
}
