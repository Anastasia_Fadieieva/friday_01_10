﻿using ConsoleApp1.Socket;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.Delegate
{
    public interface BrowserDelegate
    {
        public void GotMessage(Message message);
    }
}
