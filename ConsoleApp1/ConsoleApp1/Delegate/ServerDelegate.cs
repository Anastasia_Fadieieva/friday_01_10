﻿using ConsoleApp1.Socket;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.Delegate
{
    public interface ServerDelegate
    {
        public void Receive(Message message);

    }
}
