﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.Socket
{
    public interface SocketServerDelegate
    {
        public void Receive(Message message);

        public void GotMessage(Message message);
    }
}
